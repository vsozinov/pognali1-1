﻿using System;

namespace pognali
{
    class CreditBroker
    {
        public bool IsCreditApproved(int age)
        {
            if (age < 25)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
   //  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    class Deposit
    {
        public double CalculateFinalDeposit(double userCash, int yearsOfDeposit)
        {
            for (int i = 0; i < yearsOfDeposit; i++)
            {
                var yearPercent = userCash * 0.04;
                userCash += yearPercent;
            }
            return userCash;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var creditBroker = new CreditBroker();

            while (true)
            {
                Console.WriteLine("Введите свой возраст:");
                var ageString = Console.ReadLine();

                Console.WriteLine("Введите сумму вклада:");
                var userDepositAmountString = Console.ReadLine();
                var userDepositAmount = double.Parse(userDepositAmountString);

                Console.WriteLine("Введите срок вклада(лет):");
                var depositDurationString = Console.ReadLine();
                var depositDuration = int.Parse(depositDurationString);

                var isAgeStringValid = int.TryParse(ageString, out int age);

                if (isAgeStringValid)
                {
                    var result = creditBroker.IsCreditApproved(age);
                    Console.WriteLine($"Результат одобрения: {result}\r\n");

                    if (result)
                    {
                        var finalDeposit = new Deposit().CalculateFinalDeposit(userDepositAmount, depositDuration);
                    }
                }
                else
                {
                    Console.WriteLine("please ");
                    Console.WriteLine();
                }

            }
        }
    }
}
